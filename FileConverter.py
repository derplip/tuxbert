#!/usr/bin/env python3

import os
import sys
import subprocess


class FileConverter:

    def __init__(self, progressCallback=None):
        self.progressCallback=progressCallback

    def compressedFileToWav(self, source, target):
        print(source + " -> " + target)
        command = "sox --buffer 131072 --multi-threaded --no-glob".split(" ") + [source] + \
            "--clobber -r 32000 -b 16 -e signed-integer --no-glob".split(" ") + [target]  + \
                "remix - gain -n -1.5 bass +1 loudness -1 pad 0 0 dither".split(" ")
        #print(command)
        subprocess.run(command)
    
    def compressedFilesToWav(self, sourceFiles, targetDir):
        for idx in range(0, len(sourceFiles)):
            self.compressedFileToWav(sourceFiles[idx], targetDir + "/" + str(idx) + ".wav")
            if self.progressCallback is not None:
                self.progressCallback(idx, len(sourceFiles))

    def convertFolder(self, sourceFolder, targetFolder, sourceType='mp3'):        
        fileList = []
        for file in os.listdir(sourceFolder):
            if file.endswith(sourceType):
                #print(os.path.join(sourceFolder, file))
                fileList += [os.path.join(sourceFolder, file)]
        self.compressedFilesToWav(fileList, targetFolder)


if __name__ == "__main__":
    converter = FileConverter()
    #converter.compressedFileToWav(sys.argv[1], sys.argv[2])
    converter.convertFolder(sys.argv[1], sys.argv[2])