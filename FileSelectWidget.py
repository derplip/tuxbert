import sys

from PyQt5 import QtCore, QtGui
from PyQt5.QtWidgets import QWidget, QApplication, QFileDialog
from UI_fileselectwidget import Ui_Form

class FileSelectWidget(QWidget):

    def __init__(self, parent=None):
        QWidget.__init__(self, parent)
        self.ui = Ui_Form()
        self.ui.setupUi(self)
        self.fileList = []
        self.ui.pushButtonAddFile.clicked.connect(self.addFiles)
        self.ui.pushButtonClearList.clicked.connect(self.clearFiles)

    def addFiles(self):
        files = QFileDialog.getOpenFileNames()[0]
        if files:
            #print(files)
            self.fileList += files
            self.ui.listWidget.addItems(files)
            
    def clearFiles(self):
        self.fileList = []
        self.ui.listWidget.clear()

    def getFileList(self):
        return self.fileList


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = FileSelectWidget()
    window.show()
    sys.exit(app.exec_())
