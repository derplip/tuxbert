# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainwindow.ui'
#
# Created by: PyQt5 UI code generator 5.12
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setObjectName("label_3")
        self.horizontalLayout.addWidget(self.label_3)
        self.comboBoxTargetFolder = QtWidgets.QComboBox(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.comboBoxTargetFolder.sizePolicy().hasHeightForWidth())
        self.comboBoxTargetFolder.setSizePolicy(sizePolicy)
        self.comboBoxTargetFolder.setObjectName("comboBoxTargetFolder")
        self.horizontalLayout.addWidget(self.comboBoxTargetFolder)
        self.pushButtonSelectTarget = QtWidgets.QPushButton(self.centralwidget)
        self.pushButtonSelectTarget.setObjectName("pushButtonSelectTarget")
        self.horizontalLayout.addWidget(self.pushButtonSelectTarget)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.tabWidget = QtWidgets.QTabWidget(self.centralwidget)
        self.tabWidget.setObjectName("tabWidget")
        self.verticalLayout.addWidget(self.tabWidget)
        self.pushButtonConvert = QtWidgets.QPushButton(self.centralwidget)
        self.pushButtonConvert.setObjectName("pushButtonConvert")
        self.verticalLayout.addWidget(self.pushButtonConvert)
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.progressBarCurrent = QtWidgets.QProgressBar(self.centralwidget)
        self.progressBarCurrent.setProperty("value", 24)
        self.progressBarCurrent.setObjectName("progressBarCurrent")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.progressBarCurrent)
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.progressBarOverall = QtWidgets.QProgressBar(self.centralwidget)
        self.progressBarOverall.setProperty("value", 24)
        self.progressBarOverall.setObjectName("progressBarOverall")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.progressBarOverall)
        self.verticalLayout.addLayout(self.formLayout)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 19))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "TuxBert - Linux Manager for Hörbert"))
        self.label_3.setText(_translate("MainWindow", "Target"))
        self.pushButtonSelectTarget.setText(_translate("MainWindow", "..."))
        self.pushButtonConvert.setText(_translate("MainWindow", "Start Conversion"))
        self.label.setText(_translate("MainWindow", "Current"))
        self.label_2.setText(_translate("MainWindow", "Overall"))


