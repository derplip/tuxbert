import sys, os
import collections

from FileConverter import FileConverter

class MemoryManager:

    def __init__(self, progressCallback=None):
        self.progressCallback=progressCallback
        self.targetDir = "./tmp/tuxbert_test"
        self.buttonContents = collections.OrderedDict()
        self.converter = FileConverter()
        self.totalFileNum = 0
        self.totalFileCount = 0

    def addFiles(self, buttonIndex, files):
        if buttonIndex not in range(0, 9):
            raise Exception("Invalid button index: " + str(buttonIndex))
        self.buttonContents[buttonIndex] = files

    def clearContents(self):
        self.buttonContents.clear()

    def setTargetDir(self, dir):
        self.targetDir = dir

    def copyContents(self):
        self.getTotalFileNum()
        for key in self.buttonContents:
            dir = self.targetDir + "/" + str(key)
            self.createDirectory(dir)
            self.clearDirectory(dir)
            files = self.buttonContents[key]
            for idx in range(0 , len(files)):
                self.converter.compressedFileToWav(files[idx], dir + "/" + str(idx) + ".wav") 
                self.fileConverted(idx+1, len(files))

    def getTotalFileNum(self):
        self.totalFileNum = 0
        self.totalFileCount = 0
        for key in self.buttonContents:
            self.totalFileNum += len(self.buttonContents[key])
            
    def createDirectory(self, dir):
        if not os.path.exists(dir) or not os.path.isdir(dir):
            print("makedirs " + dir)
            os.makedirs(dir)

    def clearDirectory(self, dir):
        for f in os.listdir(dir):
            file_path = os.path.join(dir, f)
            if os.path.isfile(file_path):
                os.unlink(file_path)

    def fileConverted(self, progress, final):
        self.totalFileCount += 1
        if self.progressCallback is not None:
            self.progressCallback(progress, final, self.totalFileCount, self.totalFileNum)
