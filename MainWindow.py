#!/usr/bin/env python3
import sys
from PyQt5 import QtCore, QtGui
from PyQt5.QtWidgets import QMainWindow, QApplication, QFileDialog
from UI_mainwindow import Ui_MainWindow
from FileSelectWidget import FileSelectWidget
from MemoryManager import MemoryManager


class MainWindow(QMainWindow):

    def __init__(self, parent=None):
        QMainWindow.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.memManager = MemoryManager(self.progressCallback)

        self.ui.progressBarCurrent.setValue(0)
        self.ui.progressBarOverall.setValue(0)
        self.ui.pushButtonSelectTarget.clicked.connect(self.selectTargerDir)
        self.ui.pushButtonConvert.clicked.connect(self.convertFiles)

        self.buttonWidgets = []
        for i in range (0, 9):
            widget = FileSelectWidget()
            self.buttonWidgets.append(widget)
            self.ui.tabWidget.addTab(widget, str(i+1))

    def selectTargerDir(self):
        dir = QFileDialog.getExistingDirectory()
        if dir:
            self.ui.comboBoxTargetFolder.addItem(dir)
            self.memManager.setTargetDir(dir)

    def convertFiles(self):           
        self.initOverallProgressBar()
        for i in range(0, len(self.buttonWidgets)):
            print("Button " + str(i+1) + ": ")
            for file in self.buttonWidgets[i].getFileList():
                print("   " + file)
                self.memManager.addFiles(i, self.buttonWidgets[i].getFileList())
        self.memManager.copyContents()

    def initOverallProgressBar(self):
        num = 0
        for i in range(0, len(self.buttonWidgets)):
            num += len(self.buttonWidgets[i].getFileList())
        self.ui.progressBarOverall.setMaximum(num)

    def progressCallback(self, progressCurrent, finalCurrent, progressOverall, finalOverall):
        self.ui.progressBarCurrent.setMaximum(finalCurrent)
        self.ui.progressBarCurrent.setValue(progressCurrent)
        self.ui.progressBarOverall.setMaximum(finalOverall)
        self.ui.progressBarOverall.setValue(progressOverall)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())
