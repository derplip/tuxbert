# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'fileselectwidget.ui'
#
# Created by: PyQt5 UI code generator 5.12
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(777, 497)
        self.verticalLayout = QtWidgets.QVBoxLayout(Form)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.pushButtonAddFile = QtWidgets.QPushButton(Form)
        self.pushButtonAddFile.setObjectName("pushButtonAddFile")
        self.horizontalLayout.addWidget(self.pushButtonAddFile)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.listWidget = QtWidgets.QListWidget(Form)
        self.listWidget.setObjectName("listWidget")
        self.verticalLayout.addWidget(self.listWidget)
        self.pushButtonClearList = QtWidgets.QPushButton(Form)
        self.pushButtonClearList.setObjectName("pushButtonClearList")
        self.verticalLayout.addWidget(self.pushButtonClearList)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.pushButtonAddFile.setText(_translate("Form", "Add Files"))
        self.pushButtonClearList.setText(_translate("Form", "Clear List"))


